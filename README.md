# Projet tutoré S3 concernant les stages en Base de Données Avancée #

L'application Java devra permettre d'obtenir des inforamtions statistiques sur les stages. 

## Fonctions ##
* récupérer le nombre d'étudiants avec stage cette année, 
* récupérer le nombre d'étudiants sans stage cette année, 
* récupérer le nombre d'étudiants sans stage à une certaine date pour une année précédente choisie par l'utilisateur, 
* le nombre de stagiaires pris par chaque entreprise durant les n dernières années, 
* le nombre moyen de stagiaires encadrés par les entreprises dans les n dernières années, 
* le nombre de stages par zone géographique choisi par l'utilisateur (département, ville),  
* le nombre de stages pour toutes les zones géographiques (département, ville),  
* récupérer toutes les entreprises et leur contact ayant eu au moins un stage dans les n dernières années.

## Consignes ##

* Aucun traitement ne doit être fait en Java, et vous devez utiliser des requêtes, précompilées lorsque c'est utile, ou bien quand il s'agit de faire des calculs, des procédures ou fonctions stockées (coté serveur SGBD). Java doit être exclusivement utilisé pour présenter les informations récupérées avec Java depuis la base de données. 

* Il vous est aussi demandé de créer une table statistique, d'une seule ligne, et dont chaque colonne contiendra les résultats du calcul des statistiques décrites au dessus (sauf celles relatives aux entreprises et à leur contact). Un trigger devra surveiller les modifications des tables utilisées pour faire ces statistiques, et à chaque modification, recalculer les statistiques et mettre à jour la tables statistique avec les nouveaux calculs à l'aide de procédures stockées. 

## Compte rendu ##
Vous devrez rendre, lors de la dernière séance du module de Base de Données Avancée qui se déroulera au début du mois de janvier, exclusivement sur la plateforme pédagogique Moodle,  les déliverables suivant contenu dans un fichier .zip contenant:

* un script SQL fonctionnel de création de la structure de la base (3 points). 
* un script permettant d'insérer toutes les données test dans la base de données, permettant de remplir les tables avec des données réalistes en nombre suffisant, pour montrer le bon fonctionnement de votre application. N'hésitez pas à demander des données sur les stages en cours au secrétariat (3 points),  
* un script contenant le code commenté des procédures et fonctions stockées (3 points), 
* un script contenant le code commenté du/des triggers remplissant la table statistique à chaque modification (3 points).
* le code Java commenté en Javadoc, et une application Java fonctionnelle dont vous ferez la démonstration lors de la dernière séance du module de Base de Données Avancée qui se déroulera la première séance de janvier  (5 points). 

## Soutenance ##

il y aura lors de la dernière séance une soutenance de 20 min par groupe, où chaque membre du groupe devra exposer en 5 min, sa contribution au développement de l'application à l'aide de 4 à 5 diapositives préparées au préalable avec les autres membres de son groupe (3 points).
## Contact ##
Pour toute question, vous pouvez contacter le responsable de la matière à l'adresse suivante : 
nicolas.ferey@u-psud.fr.   