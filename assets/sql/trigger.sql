-- Trigger g�rant les insertions

  -- Sur la table obEtudiant
create or replace trigger trg_ins_etudiant
  before insert
  on ob_etudiant
  for each row
begin
  update statistique set NbEtu = NbEtu + 1;
  update statistique set NbEtuSansStage = NbEtuSansStage + 1;
end;
/

  -- Sur la table obStagiaire
create or replace trigger trg_ins_stagiaire
  after insert
  on stagiaire
  for each row
begin
  update statistique set NbEtuAvecStage = NbEtuAvecStage + 1;
  update statistique set NbEtuSansStage = NbEtuSansStage - 1;
end;
/

  -- Sur la table obStage
create or replace trigger trg_ins_stage
  after insert
  on ob_stage
  for each row
begin
  update statistique set NbStage = NbStage + 1;
end;
/

-- Trigger g�rant les supressions

  -- Sur la table obEtudiant
create or replace trigger trg_sup_etudiant
  after delete
  on ob_etudiant
  for each row
begin
  update statistique set NbEtu = NbEtu - 1;
end;
/

  -- Sur la table obStagiaire
create or replace trigger trg_sup_stagiaire
  after delete
  on stagiaire
  for each row
begin
  update statistique set NbEtuAvecStage = NbEtuAvecStage - 1;
  update statistique set NbEtuSansStage = NbEtuSansStage + 1;
end;
/

  -- Sur la table obStage
create or replace trigger trg_sup_stage
  after delete
  on ob_stage
  for each row
begin
  update statistique set NbStage = NbStage - 1;
end;
/