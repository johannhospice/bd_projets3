insert into statistique
values      (0,0,0,0);

insert into ob_zone (id,ville,departement)
values      (0,'paris',75);
insert into ob_zone (id,ville,departement)
values      (1,'massy',91);
insert into ob_zone (id,ville,departement)
values      (2,'orsay',91);
insert into ob_zone (id,ville,departement)
values      (3,'evry',91);
insert into ob_zone (id,ville,departement)
values      (4,'creteil',94);
insert into ob_zone (id,ville,departement)
values      (5,'vitry-sur-seine',94);
insert into ob_zone (id,ville,departement)
values      (6,'rungis',94);
insert into ob_zone (id,ville,departement)
values      (7,'saint-denis',93);

insert into ob_entreprise (id,nom,zone)
select      0,'thales', ref(z) from ob_zone z where  id = 6;
insert into ob_entreprise (id,nom,zone)
select      1,'ratp', ref(z)
                       from   ob_zone z
                       where  id = 5;
insert into ob_entreprise (id,nom,zone)
select      2,'google', ref(z)
                         from   ob_zone z
                         where  id = 0;
insert into ob_entreprise (id,nom,zone)
select      3,'edf', ref(z)
                      from   ob_zone z
                      where  id = 0;
insert into ob_entreprise (id,nom,zone)
select      4,'generali', ref(z)
                      from   ob_zone z
                      where  id = 0;
insert into ob_entreprise (id,nom,zone)
select      5,'murex', ref(z)
                      from   ob_zone z
                      where  id = 0;
insert into ob_entreprise (id,nom,zone)
select      6,'axa', ref(z)
                      from   ob_zone z
                      where  id = 0;
insert into ob_entreprise (id,nom,zone)
select      7,'ubisoft', ref(z)
                      from   ob_zone z
                      where  id = 0;
insert into ob_entreprise (id,nom,zone)
select      8,'sony', ref(z)
                      from   ob_zone z
                      where  id = 0;
insert into ob_entreprise (id,nom,zone)
select      9,'ubisoft', ref(z)
                      from   ob_zone z
                      where  id = 0;
                      
insert into ob_etudiant (id,prenom,nom,zone)
select      0,'Nestor','Bubulle', ref(z)
                                   from   ob_zone z
                                   where  id = 2;
insert into ob_etudiant (id,prenom,nom,zone)
select      1,'Goku','Son',ref(z)
                             from   ob_zone z
                             where  id = 4;
insert into ob_etudiant (id,prenom,nom,zone)
select      2,'Johann','Hospice', ref(z)
                                   from   ob_zone z
                                   where  id = 3;
insert into ob_etudiant (id,prenom,nom,zone)
select      3,'Jophilga','Dississa', ref(z)
                                      from   ob_zone z
                                      where  id = 3;
insert into ob_etudiant (id,prenom,nom,zone)
select      4,'Lea','Organa', ref(z)
                               from   ob_zone z
                               where  id = 4;
insert into ob_etudiant (id,prenom,nom,zone)
select      5,'Eva','Loca', ref(z)
                             from   ob_zone z
                             where  id = 7;
insert into ob_etudiant (id,prenom,nom,zone)
select      6,'Will','Smith', ref(z)
                             from   ob_zone z
                             where  id = 2;
insert into ob_etudiant (id,prenom,nom,zone)
select      7,'Chelsea','Bern', ref(z)
                             from   ob_zone z
                             where  id = 2;
insert into ob_etudiant (id,prenom,nom,zone)
select      8,'Flobio','Vilaini', ref(z)
                             from   ob_zone z
                             where  id = 5;
insert into ob_etudiant (id,prenom,nom,zone)
select      9,'Nico','Bellic', ref(z)
                             from   ob_zone z
                             where  id = 0;
insert into ob_etudiant (id,prenom,nom,zone)
select      10,'Carl','Johnson', ref(z)
                             from   ob_zone z
                             where  id = 1;
insert into ob_etudiant (id,prenom,nom,zone)
select      11,'Chelsea','Baye', ref(z)
                             from   ob_zone z
                             where  id = 6;
                             
insert into ob_stage (id,annee,entreprise)
select      0,2015,ref(z)
                     from   ob_entreprise z
                     where  id = 1;
insert into ob_stage (id,annee,entreprise)
select      1,2014, ref(z)
                     from   ob_entreprise z
                     where  id = 2;
insert into ob_stage (id,annee,entreprise)
select      2,2014, ref(z)
                     from   ob_entreprise z
                     where  id = 2;
insert into ob_stage (id,annee,entreprise)
select      3,2013, ref(z)
                     from   ob_entreprise z
                     where  id = 3;
insert into ob_stage (id,annee,entreprise)
select      4,2010, ref(z)
                     from   ob_entreprise z
                     where  id = 0;
insert into ob_stage (id,annee,entreprise)
select      5,2016, ref(z)
                     from   ob_entreprise z
                     where  id = 6;
insert into ob_stage (id,annee,entreprise)
select      6,2016, ref(z)
                     from   ob_entreprise z
                     where  id = 6;
insert into ob_stage (id,annee,entreprise)
select      7,2016, ref(z)
                     from   ob_entreprise z
                     where  id = 9;
insert into ob_stage (id,annee,entreprise)
select      8,2016, ref(z)
                     from   ob_entreprise z
                     where  id = 1;
insert into ob_stage (id,annee,entreprise)
select      9,2016, ref(z)
                     from   ob_entreprise z
                     where  id = 2;
insert into ob_stage (id,annee,entreprise)
select      10,2016, ref(z)
                     from   ob_entreprise z
                     where  id = 5;
insert into ob_stage (id,annee,entreprise)
select      11,2015, ref(z)
                     from   ob_entreprise z
                     where  id = 3;

insert into stagiaire (id,stage,etudiant)
select      0,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 5 and s.id=1;
insert into stagiaire (id,stage,etudiant)
select      1,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 3 and s.id = 2;
insert into stagiaire (id,stage,etudiant)
select      2,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 1 and s.id = 3;
insert into stagiaire (id,stage,etudiant)
select      3,ref(s), ref(z)
                  from   ob_etudiant z , ob_stage s
                  where  z.id = 2 and s.id = 4;
insert into stagiaire (id,stage,etudiant)
select      4,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 0 and s.id = 5; 
insert into stagiaire (id,stage,etudiant)
select      5,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 6 and s.id = 6; 
insert into stagiaire (id,stage,etudiant)
select      6,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 8 and s.id = 7; 
insert into stagiaire (id,stage,etudiant)
select      7,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 9 and s.id = 8; 
insert into stagiaire (id,stage,etudiant)
select      8,ref(s), ref(z)
                  from   ob_etudiant z, ob_stage s
                  where  z.id = 7 and s.id = 10; 