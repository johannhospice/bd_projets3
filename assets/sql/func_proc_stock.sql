-- R�cup�rer le nombre d'�tudiants avec stage � une ann�e donn�e
create or replace FUNCTION NB_ETU_AVEC_STAGE (ANNEE IN NUMBER) RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from stagiaire s
  where s.stage.Annee = ANNEE;
  RETURN nb;
END NB_ETU_AVEC_STAGE;
/

-- R�cup�rer le nombre d'�tudiants sans stage � une ann�e donn�e
create or replace FUNCTION NB_ETU_SANS_STAGE (ANNEE IN NUMBER) RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from ob_Etudiant e
  where e.id NOT IN ( select s.Etudiant.id
                                from Stagiaire s
                                where s.Stage.Annee = ANNEE );
  RETURN nb;
END NB_ETU_SANS_STAGE;
/

-- R�cup�rer le nombre de stagiaires pris par une entreprise donn�e durant les n derni�res ann�es
create or replace FUNCTION NB_STAGIAIRE_ENT (nomEnt VARCHAR2, n IN NUMBER) RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from Stagiaire s,DUAL
  where s.Stage.Entreprise.nom = nomEnt and
  s.Stage.Annee <= TO_CHAR(SYSDATE,'YYYY') and s.Stage.Annee > (TO_CHAR(SYSDATE,'YYYY')-nb);
  RETURN nb;
END NB_STAGIAIRE_ENT;
/

-- R�cup�rer le nombre de stagiaires pris par entreprise durant les n derni�res ann�es
create or replace FUNCTION NB_STAGIAIRE (n IN NUMBER) RETURN NUMBER AS
nb NUMBER;
somme NUMBER := 0;
nomEnt VARCHAR2(30);
Cursor nom_ents IS
    select e.nom
    from ob_Entreprise e
    where e.id IN ( select s.stage.Entreprise.id
                                from Stagiaire s );
BEGIN
  OPEN nom_ents;
  LOOP
    FETCH nom_ents INTO nomEnt;
    EXIT WHEN nom_ents%NOTFOUND;
    somme := somme + NB_STAGIAIRE_ENT(nomEnt, n);
  END LOOP;
  CLOSE nom_ents;
  nb := somme / (nom_ents%rowCount);
  RETURN nb;
END NB_STAGIAIRE;
/

-- Obtenir le nombre moyen de stagiaires encadr�s par les entreprises dans les n derni�res ann�es
create or replace FUNCTION NB_STAGIAIRE_MOY (n IN NUMBER) RETURN NUMBER AS
somme NUMBER := 0;
moy NUMBER;
cpt int := 0;
actual_year int := 2016;
BEGIN
  select TO_CHAR(SYSDATE,'YYYY') into actual_year from DUAL;
  LOOP
    somme := somme + NB_ETU_AVEC_STAGE(actual_year-cpt) ;
    cpt := cpt + 1;
    EXIT WHEN cpt > (n-1);
  END LOOP;
  moy := somme/n;
  RETURN moy;
END NB_STAGIAIRE_MOY;
/

-- Obtenir le nombre de stages par zone g�ographique choisi par l'utilisateur (d�partement, ville)
create or replace FUNCTION NB_STAGE_ZONE (dep NUMBER, ville VARCHAR2) RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from ob_Stage s
  where s.Entreprise.zone.Ville = ville and
  s.Entreprise.zone.departement = dep;
  RETURN nb;
END NB_STAGE_ZONE;
/
create or replace FUNCTION NB_STAGE_ZONE_DEP (dep NUMBER) RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from ob_Stage s
  where s.Entreprise.zone.departement = dep;
  RETURN nb;
END NB_STAGE_ZONE_DEP;
/
create or replace FUNCTION NB_STAGE_ZONE_VIL (ville VARCHAR2) RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from ob_Stage s
  where s.Entreprise.zone.Ville = ville ;
  RETURN nb;
END NB_STAGE_ZONE_VIL;
/

-- le nombre de stages pour toutes les zones g�ographiques (d�partement, ville)
create or replace FUNCTION NB_STAGE_TOUTE_ZONE RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from ob_Stage s;
  RETURN nb;
END NB_STAGE_TOUTE_ZONE;
/

-- r�cup�rer toutes les entreprises et leur contact ayant eu au moins un stage dans les n derni�res ann�es
create or replace PROCEDURE ENTREPRISE_AND_CONTACT (n IN NUMBER) AS
nomEnt VARCHAR2(30);
Cursor lesEntreprises IS
    select s.stage.entreprise.nom, s.etudian.nom, s.etudiant.prenom
    from Stagiaire s
    where s.Stage.Annee > TO_CHAR(SYSDATE,'YYYY')-n
    order by s.entreprise.nom;
BEGIN
  DBMS_OUTPUT.PUT_LINE('Voici les entreprises et leur contact ayant eu au moins un stage dans les '|| n ||' derni?res ann�es :');

  FOR ligneCurseur IN lesEntreprises
  LOOP
    DBMS_OUTPUT.PUT_LINE('Entreprise : ' || ligneCurseur.stage.entreprise.nom ||', Contact : '|| ligneCurseur.etudian.nom||' '|| ligneCurseur.etudian.prenom);
  END LOOP;
END ENTREPRISE_AND_CONTACT;
/
select ENTREPRISE_AND_CONTACT(5) from dual;

-- AUTRES FONCTIONS
  -- Donne le nombre total d'�tudiants
create or replace FUNCTION NB_ETUDIANT RETURN NUMBER AS
nb NUMBER;
BEGIN
  select count(*) into nb
  from ob_Etudiant e;
  RETURN nb;
END NB_ETUDIANT;