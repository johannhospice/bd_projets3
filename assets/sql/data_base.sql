-- ZONE
create or replace type ob_zone_ty as object(
  id int,
  ville VARCHAR2(15),
  departement NUMBER(2)
);
/
create table ob_zone of ob_zone_ty(id PRIMARY KEY);

-- ETUDIANT
create or replace type ob_etudiant_ty as object(  
  id int,
  nom VARCHAR2(25),
  prenom VARCHAR2(25),
  zone ref ob_zone_ty
);
/
create table ob_etudiant of ob_etudiant_ty(id PRIMARY KEY);

-- ENTREPRISE
create or replace type ob_entreprise_ty as object(
  id int,
  nom VARCHAR2(25),
  zone ref ob_zone_ty
);
/
create table ob_entreprise of ob_entreprise_ty(id PRIMARY KEY);

-- STAGE
create or replace type ob_stage_ty as object(
  id int,
  annee NUMBER(4),
  entreprise ref ob_entreprise_ty 
);
/
create table ob_stage of ob_stage_ty(id PRIMARY KEY);

-- STAGIAIRE
create table stagiaire (
  id int,
  etudiant ref ob_etudiant_ty,
  stage ref ob_stage_ty,
  primary key(id)
);
/

-- STATISTIQUE
create table statistique(
  NbEtu NUMBER DEFAULT 0,
  NbEtuAvecStage NUMBER DEFAULT 0,
  NbEtuSansStage NUMBER DEFAULT 0,
  NbStage NUMBER DEFAULT 0,
  Nb
  check(nbEtu >= 0 and NbEtuAvecStage >= 0 and NbEtuSansStage >= 0 and NbStage >= 0)
);
/