package tools;

import gui.frames.SchedulerFrame;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import app.Application;

/**
 * Sheduler est le point de lancement de l'application, il instancie le modele
 * et la vue @see Application
 */

public class Scheduler {

	public static void main(String[] args) {

		String[] options = { "Oui", "Non", "Annuler" };
		if (0 == JOptionPane.showOptionDialog(SchedulerFrame.instance(),
				"Souhaitez vous utiliser une connexion spécifique ?", "Connexion",
				javax.swing.JOptionPane.YES_NO_OPTION, javax.swing.JOptionPane.QUESTION_MESSAGE, null, options,
				options[2])) {
			Config.login = JOptionPane.showInputDialog("Saisie login");
			Config.passwd = JOptionPane.showInputDialog("Saisie psswd");
		}
		Application.instance();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				SchedulerFrame.instance().setVisible(true);
			}
		});
	}
}
