package app;

import java.sql.Connection;
import static tools.Config.*;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import gui.frames.SchedulerFrame;
/**
 * Application est la partie principale du modele contenant le gestionnaire de requete @see QueriesManager
 */
public class Application {
	
  	private String url = "jdbc:oracle:thin:"+login+"/"+passwd+"@oracle.iut-orsay.fr:1521:etudom";
  	private static Application app;
    private static QueriesManager queries;

    private Application(){
        try {
			queries = new QueriesManager(openConnection(url));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(SchedulerFrame.instance(), "erreur lors de la connexion", "fatal error",JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
    }
    public int getIntResultRequest(int i2, int args) throws SQLException {
        return queries.getIntResultRequest(i2, args);
    }
    public int getStringResultRequest(int i2, String args) throws SQLException {
        return queries.getStringResultRequest(i2, args);
    }
    /*
    public int getMultiResultRequest(int i2, int a2, String v2) throws SQLException {
        return queries.getMultiResultRequest(i2, a2, v2);
    }    
    */
	/**
	 * Permet la connexion a la base de donn�es
	 */
    public Connection openConnection(String url) throws Exception {
        Connection co = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            co = DriverManager.getConnection(url);
            System.out.println("Connexion ouverte!\n");
        } catch (ClassNotFoundException e) {
            System.out.println("il manque le driver oracle");
        	throw new Exception();
        } catch (SQLException e) {
            System.out.println("impossible de se connecter ï¿½ l'url : " + url);
        	throw new Exception();
        }
        return co;
    }
	/**
	 * singleton
	 */
    public static Application instance(){
    	if(app == null){
    		app=new Application();
    	}
    	return app;
    }


}