package app;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Permet de gérer un groupe de requete
 */
public class QueriesManager {

	private final Connection co;

	private ArrayList<CallableStatement> cstS;

	public QueriesManager(Connection co) {
		this.co = co;
		cstS = new ArrayList<CallableStatement>();
		LoadQueries();
	}

	/**
	 * Charge une liste des fonction compil�
	 * 
	 * @see ArrayList<CallableStatement>
	 */
	private void LoadQueries() {
		try {
			// R�cup�rer le nombre d'�tudiants avec stage � une ann�e donn�e
			cstS.add(co.prepareCall("{? = call NB_ETU_AVEC_STAGE (?) }"));

			// R�cup�rer le nombre d'�tudiants sans stage � une ann�e donn�e
			cstS.add(co.prepareCall("{? = call NB_ETU_SANS_STAGE (?) }"));

			// R�cup�rer le nombre de stagiaires pris par entreprise donn�e
			// durant les n derni�res ann�es
			cstS.add(co.prepareCall("{? = call NB_STAGIAIRE_ENT (?) }"));

			// Obtenir le nombre moyen de stagiaires encadr�s par les
			// entreprises dans les n derni�res ann�es
			cstS.add(co.prepareCall("{? = call NB_STAGIAIRE_MOY (?) }"));

			// Obtenir le nombre de stages par zone g�ographique choisi par
			// l'utilisateur (d�partement, ville)
			cstS.add(co.prepareCall("{? = call NB_STAGE_ZONE_DEP (?) }"));
			cstS.add(co.prepareCall("{? = call NB_STAGE_ZONE_VIL (?) }"));

			// le nombre de stages pour toutes les zones g�ographiques
			// (d�partement, ville)
			cstS.add(co.prepareCall("{? = call NB_STAGE_TOUTE_ZONE }"));

			// r�cup�rer toutes les entreprises et leur contact ayant eu au
			// moins un stage dans les n derni�res ann�es
			cstS.add(co.prepareCall("{call ENTREPRISE_AND_CONTACT (?) }"));

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public int getMultiResultRequest(int i, int args, String arg2) throws
	 * SQLException { cstS.get(i).setInt(2, args); cstS.get(i).setString(2,
	 * arg2); return getResultRequest(cstS.get(i)); }
	 */
	public int getIntResultRequest(int i, int args) throws SQLException {
		if (i != 6)
			if (i != 7)
				cstS.get(i).setInt(2, args);
			else
				cstS.get(i).setInt(1, args);
		return getResultRequest(cstS.get(i));
	}

	public int getStringResultRequest(int i, String args) throws SQLException {
		cstS.get(5).setString(2, args);
		return getResultRequest(cstS.get(i));
	}

	/**
	 * Fonction permettant d'executer une requete compil�, prend en parametre le
	 * retour d'une de ces fonction
	 * 
	 * @see getStringResultRequest
	 * @see getMultiResultRequest
	 * @see getIntResultRequest
	 * @param cst
	 * @return Integer
	 * @throws SQLException
	 */
	private int getResultRequest(CallableStatement cst) throws SQLException {
		cst.registerOutParameter(1, java.sql.Types.NUMERIC);
		cst.execute();
		return cst.getInt(1);
	}
}