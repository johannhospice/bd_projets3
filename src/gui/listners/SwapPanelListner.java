package gui.listners;

import gui.frames.SchedulerFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * un controleur permettant de pr�parer les changement de vue de l'application
 */
public class SwapPanelListner implements ActionListener {
	private JPanel newPanel;
	private JPanel oldPanel;

	public SwapPanelListner( JPanel oldPanel, JPanel newPanel) {
		this.oldPanel = oldPanel;
		this.newPanel = newPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		SchedulerFrame.instance().swapPanel(oldPanel, newPanel);
	}

}
