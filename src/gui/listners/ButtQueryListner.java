/**
 * 
 */
package gui.listners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Calendar;

import javax.swing.JOptionPane;
import app.Application;
import gui.frames.SchedulerFrame;

/**
 * Evenement destin� � l'actionnement d'un bouton et permettant l'ouverture d'un
 * cas de r�cup�ration de donn�es statistiques
 */
public class ButtQueryListner implements ActionListener {
	private int type;

	/**
	 * R�cup�re le type du bouton d'�terminant son utilisation
	 * @param type
	 */
	public ButtQueryListner(int type) {
		this.type = type;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String msg = new String();
		int a = Calendar.getInstance().get(Calendar.YEAR);
		String[] options ={ "Oui", "Non" } ;
		try {
			switch (type) {
			case 0:
				JOptionPane.showMessageDialog(SchedulerFrame.instance(),
						"en " + a + ": " + Application.instance().getIntResultRequest(0, a),
						"nombre d'�tudiants avec stage", JOptionPane.INFORMATION_MESSAGE);
				break;
			case 1:
				JOptionPane.showMessageDialog(SchedulerFrame.instance(),
						"en " + a + ": " + Application.instance().getIntResultRequest(1, a),
						"nombre de d'�tudiant sans stage", JOptionPane.INFORMATION_MESSAGE);
				break;
			case 2:
				a = Integer.parseInt(JOptionPane.showInputDialog("Saisir un retrait d'ann�e"));

				JOptionPane.showMessageDialog(SchedulerFrame.instance(),
						"� partir de " + (Calendar.getInstance().get(Calendar.YEAR) - a) + ": "
								+ Application.instance().getIntResultRequest(1, a),
						"nombre d'�tudiants sans stage", JOptionPane.INFORMATION_MESSAGE);
				break;
			case 3:
				JOptionPane.showMessageDialog(null,
						"Op�ration non impl�ment� en java,\nvoir le code sql de la fonction NB_STAGIAIRE_ENT", "error",
						JOptionPane.WARNING_MESSAGE);
				/*
				 * a = Integer.parseInt(JOptionPane.showInputDialog(
				 * "Saisir une ann�e")); msg =
				 * "nombre de stagiaires pris par chaque entreprise durant les "
				 * + a + " dernieres ann�es:\n" +
				 * Application.instance().getIntResultRequest(2, a);
				 */
				break;
			case 4:
				a = Integer.parseInt(JOptionPane.showInputDialog(SchedulerFrame.instance(), "Saisir un retrait d'ann�e",
						"nombre moyen de stagiaires", JOptionPane.PLAIN_MESSAGE));

				JOptionPane.showMessageDialog(SchedulerFrame.instance(),
						"� partir de " + (Calendar.getInstance().get(Calendar.YEAR) - a) + ": "
								+ Application.instance().getIntResultRequest(3, a),
						"nombre moyen de stagiaires", JOptionPane.INFORMATION_MESSAGE);
				break;
			case 5:

				options[0] = "Ville";
				options[1] = "Departement" ;
				int res = JOptionPane.showOptionDialog(SchedulerFrame.instance(),
						"Dans quel type de zone souhaitez vous faire la recherche ?", "nombre de stage par zone",
						javax.swing.JOptionPane.YES_NO_OPTION, javax.swing.JOptionPane.QUESTION_MESSAGE, null, options,
						options[0]);

				if (1 == res) {
					a = Integer.parseInt(JOptionPane.showInputDialog(SchedulerFrame.instance(),
							"Saisir  un departement", "nombre de stage par zone", JOptionPane.PLAIN_MESSAGE));
	
					JOptionPane.showMessageDialog(SchedulerFrame.instance(),
							"dans la zone du" + a + ": " + Application.instance().getIntResultRequest(4, a),
							"nombre de stage par zone", JOptionPane.INFORMATION_MESSAGE);
				} else {
					String v = JOptionPane.showInputDialog(SchedulerFrame.instance(), "Saisir une ville",
							"nombre de stage par zone", JOptionPane.PLAIN_MESSAGE);
					JOptionPane.showMessageDialog(SchedulerFrame.instance(),
							"dans la zone " + v + ": " + Application.instance().getStringResultRequest(5, v),
							"nombre de stage par zone", JOptionPane.INFORMATION_MESSAGE);
				}
				break;
			case 6:
				JOptionPane.showMessageDialog(SchedulerFrame.instance(),
						"dans toutes les zone: " + Application.instance().getIntResultRequest(6, 0) +" stages",
						"nombre de stages pour toutes les zones g�ographiques", JOptionPane.INFORMATION_MESSAGE);
				break;
			case 7:
				if(0==JOptionPane.showOptionDialog(SchedulerFrame.instance(),
					"Probleme avec la fonction sql ENTREPRISE_AND_CONTACT,\n �tes vous s�r de vouloir continuer", "Probleme",
					javax.swing.JOptionPane.YES_NO_OPTION, javax.swing.JOptionPane.WARNING_MESSAGE, null, options,
					options[1])){			
				  a = Integer.parseInt(JOptionPane.showInputDialog(
				  "Saisir une ann�e")); msg =
				  " toutes les entreprises et leur contact ayant eu au moins un stage dans les "
				  +a+" derni�res ann�es \n" +
				  Application.instance().getIntResultRequest(7, a);
				  JOptionPane.showMessageDialog(null, msg); break;
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "error", JOptionPane.WARNING_MESSAGE);
		}
	}

}
