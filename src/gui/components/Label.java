package gui.components;

import java.awt.Font;
import javax.swing.JLabel;
/**
 * Redefinition de la class @see JLabel, permettant de mieux g�rer le positionnement, les dimentions et syles
 */
public class Label extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public double height;
	public double width;

	public Label(String text, int style, int size) {
		setLabel(text,style,size);
	}
	public Label setLabel(String text, int style, int size) {
		setText(text);
		Font font = new Font("", style, size);
		setFont(font);
		height = getPreferredSize().getHeight();
		width = getPreferredSize().getWidth();
		return this;
	}

	public void setPosition(double width, double height) {
		setBounds((int) width, (int) height, getPreferredSize().width,
				getPreferredSize().height);
	}
}
