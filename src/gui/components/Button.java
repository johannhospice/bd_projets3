package gui.components;

import java.awt.event.ActionListener;
import javax.swing.JButton;
/**
 * Bouton issue de la class @see JButton, permet d'�ffectuer certaines op�rations plus ais�met
 */
public class Button extends JButton {
	private static final long serialVersionUID = 1L;
	public double height;
	public double width;

	public Button(String text) {
		this.setText(text);
	}

	public Button(String text,ActionListener action) {
		addActionListener(action);
		setText(text);
	}
/**
 * assigne un texte 
 */
	@Override
	public void setText(String text) {
		super.setText(text);
		height = getPreferredSize().getHeight();
		width = getPreferredSize().getWidth();
	}
/**
 * Permet de positionner le bouton
 * @param width
 * @param height
 */
	public void setPosition(double width, double height) {
		setBounds((int) width, (int) height, getPreferredSize().width,
				getPreferredSize().height);
	}

}
