package gui.widgets;

import gui.frames.SchedulerFrame;
import gui.listners.ButtQueryListner;
import gui.listners.SwapPanelListner;
import tools.Config;
import gui.components.Button;
import gui.components.Label;

import javax.swing.*;

import static tools.Config.*;

import java.awt.GridLayout;

/**
 * PanelBuilder permet la cr�ation et le l'envoie d'un panel
 */
public class PanelBuilder {
	/**
	 * cr�ation de l'�cran d'accueil
	 */
	public static JPanel HomePanel() {
		JPanel panel = new JPanel();
		Button enterBut = new Button("Enter",
				new SwapPanelListner(panel, SchedulerFrame.instance().getPanel(Config.MENU)));
		Label titleLab = new Label("Information Statistique ", 1, FONT_TITLE_0);
		Label subTitleLab = new Label("des stages", 1, FONT_TITLE_0);

		enterBut.setBounds(0, (int) (HEIGHT_FRAME / 4 * 3.26), WIDTH_FRAME - 2, HEIGHT_FRAME / 7);
		titleLab.setPosition((WIDTH_FRAME - titleLab.width) / 2, HEIGHT_FRAME / 2 - titleLab.height * 1.8f);
		subTitleLab.setPosition((WIDTH_FRAME - subTitleLab.width) / 2, HEIGHT_FRAME / 2 - subTitleLab.height * 0.9f);

		panel.add(enterBut);
		panel.add(titleLab);
		panel.add(subTitleLab);

		panel.setLayout(null);
		return panel;
	}

	/**
	 * Cr�ation de l'�cran de menu
	 */
	public static JPanel MenuPanel() {
		String tab[] = { "nombre d'�tudiants avec stage cette ann�e", "nombre d'�tudiants sans stage cette ann�e",
				"nombre d'�tudiants sans stage � une certaine date pour une ann�e pr�c�dente choisie par l'utilisateur",
				"nombre de stagiaires pris par chaque entreprise durant les n derni�res ann�es",
				"nombre moyen de stagiaires encadr�s par les entreprises dans les n derni�res ann�es",
				"nombre de stages par zone g�ographique choisi par l'utilisateur (d�partement, ville)",
				"nombre de stages pour toutes les zones g�ographiques (d�partement, ville)",
				"toutes les entreprises et leur contact ayant eu au moins un stage dans les n derni�res ann�es" };
		GridLayout layout = new GridLayout(4, 4);
		JPanel panel = new JPanel(layout);
		layout.setHgap(15);
		layout.setVgap(15);

		for (int i = 0; i < tab.length; i++) {
			Button but = new Button("<HTML><BODY>" + tab[i] + "</BODY></HTML>");
			but.setPosition(WIDTH_FRAME / 4, HEIGHT_FRAME / 4);
			but.addActionListener(new ButtQueryListner(i));
			panel.add(but);
		}
		return panel;
	}
}
