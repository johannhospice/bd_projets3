package gui.frames;

import gui.widgets.PanelBuilder;
import tools.Config;

import static tools.Config.*;

import java.util.HashMap;
import javax.swing.*;
/**
 * SchedulerFrame est la fenetre de l'application, il contiendra des panels cr�er avec PanelBuilder
 */
public class SchedulerFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private final String title = "Information statistique des stages";
	private HashMap<String, JPanel> panels;
/**
 * @return JPanel
 */
	public JPanel getPanel(String panel) {
		return panels.get(panel);
	}
/**
 * Initialisation des panels
 */
	protected void setupUI() {
		panels = new HashMap<>();
		panels.put(Config.MENU, PanelBuilder.MenuPanel());
		panels.put(Config.HOME, PanelBuilder.HomePanel());
		swapPanel(null, getPanel(Config.HOME));
	}

	private SchedulerFrame() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle(title);
		setResizable(false);
		setBounds(250, 250, WIDTH_FRAME, HEIGHT_FRAME);
	}
/**
 * Permet changer le panel qui est afficher dans la fenetre
 * @param oldPanel
 * @param newPanel
 */
	public void swapPanel(JPanel oldPanel, JPanel newPanel) {
		if (oldPanel != null)
			remove(oldPanel);
		add(newPanel);
		validate();
		update(getGraphics());
	}
	private static SchedulerFrame frame = null;

/**
 * singleton
 */
	public static SchedulerFrame instance() {
		if (frame == null) {
			frame = new SchedulerFrame();
			frame.setupUI();
		}
		return frame;
	}
}
